using Godot;

namespace gsrs.Player;

public partial class CameraController : Node
{
	[Signal]
	public delegate void SetCamRotationEventHandler(float rotation);

	[Export] public float Sensitivity = 0.001f;
	[Export] public Node3D Head;
	private Tween _tween;
	private Camera3D _cam;

	public override void _Ready()
	{
		_cam = Head.GetNode<Camera3D>("Camera3D");

		Input.MouseMode = Input.MouseModeEnum.Captured;
	}

	public override void _Input(InputEvent @event)
	{
		if (@event is not InputEventMouseMotion mouseMotion) return;
		Head.RotateY(-mouseMotion.Relative.X * Sensitivity);
		_cam.RotateX(-mouseMotion.Relative.Y * Sensitivity);

		var cameraRotation = _cam.Rotation;
		cameraRotation.X = Mathf.Clamp(cameraRotation.X, Mathf.DegToRad(-90f), Mathf.DegToRad(45f));
		_cam.Rotation = cameraRotation;

		EmitSignal(nameof(SetCamRotation), Head.Rotation.Y);
		// EmitSignal(nameof(SetMovementDirection), head.GlobalTransform.Basis);
	}

	private void OnSetMovementState(MovementState movementState)
	{
		_tween?.Kill();
		_tween = _cam.CreateTween();
		_tween.TweenProperty(_cam, "fov", movementState.CameraFov, 0.5f).SetTrans(Tween.TransitionType.Sine).SetEase(Tween.EaseType.Out);
	}
}

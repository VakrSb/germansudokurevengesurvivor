using System;
using System.Linq;
using Godot;

namespace gsrs.Player;

public partial class MovementController : Node
{
    [Export] public Player Player { get; set; }
    [Export] public Node3D MeshRoot;
    [Export] public float RotationSpeed = 8;
    [Export] public float FallGravity = 45;

    private Vector3 _direction;
    private Vector3 _velocity;
    private float _acceleration;
    private float _speed;
    private float _camRotation;
    private float _jumpGravity;

    public override void _Ready()
    {
        _jumpGravity = FallGravity;
    }

    public override void _PhysicsProcess(double delta)
    {
        _velocity.X = _speed * _direction.Normalized().X;
        _velocity.Z = _speed * _direction.Normalized().Z;

        if (!Player.IsOnFloor())
        {
            if (_velocity.Y >= 0) 
                _velocity.Y -= _jumpGravity * (float)delta;
            else 
                _velocity.Y -= FallGravity * (float)delta;
        }
        
        Player.Velocity = Player.Velocity.Lerp(_velocity, _acceleration * (float)delta);
        
        /*if (_direction != Vector3.Zero)
        {
            _velocity.X = _direction.X * _speed;
            _velocity.Z = _direction.Z * _speed;
        }
        else
        {
            _velocity.X = Mathf.MoveToward(_velocity.X, 0,_acceleration);
            _velocity.Z = Mathf.MoveToward(_velocity.Z, 0,_acceleration);
        }
        Player.Velocity = _velocity;*/
        
        Player.MoveAndSlide();
    }
    
    private void Jump(JumpState jumpState)
    {
        _velocity.Y = 2 * jumpState.JumpHeight / jumpState.ApexDuration;
        _jumpGravity = _velocity.Y / jumpState.ApexDuration;
    }

    private void OnSetMovementState(MovementState movementState)
    {
        _speed = movementState.MovementSpeed;
        _acceleration = movementState.Acceleration;
    }

    private void OnSetMovementDirection(Vector3 dir)
    {
        _direction = dir.Rotated(Vector3.Up, _camRotation);
    }
    private void OnSetCamRotation(float rotation)
    {
        _camRotation = rotation;
    }
}
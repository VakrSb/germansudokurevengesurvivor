using Godot;

namespace gsrs.Player;

[GlobalClass]
public partial class JumpState : Resource
{
	[Export] public float JumpHeight { get; set; }
	[Export] public float ApexDuration { get; set; }
}

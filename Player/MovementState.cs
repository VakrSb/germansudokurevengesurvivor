using Godot;

namespace gsrs.Player;

[GlobalClass]
public partial class MovementState : Resource
{
	[Export] public int Id { get; set; }
	[Export] public float MovementSpeed { get; set; }
	[Export] public float Acceleration { get; set; }
	[Export] public float CameraFov { get; set; }

	public MovementState()
	{
		Acceleration = 6;
		CameraFov = 75;
	}
}

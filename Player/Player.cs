using System;
using Godot;

namespace gsrs.Player;

public partial class Player : CharacterBody3D
{
	[Export] public Godot.Collections.Dictionary MovementStates { get; set; }
	[Export] public Godot.Collections.Dictionary JumpStates { get; set; }
	[Export] public int MaxAirJumps { get; set; }
	[Signal] public delegate void SetMovementStateEventHandler(MovementState movementState);
	[Signal] public delegate void SetMovementDirectionEventHandler(Vector3 direction);
	[Signal] public delegate void PressedJumpEventHandler(JumpState jumpState);

	private Vector3 direction;
	private int _airJumpCounter = 0;
	
	public override void _Ready()
	{
		EmitSignal(nameof(SetMovementState), MovementStates["stand"]);
	}
	
	public override void _PhysicsProcess(double delta)
	{
		if (IsMovement())
			EmitSignal(nameof(SetMovementDirection), direction);
		if (IsOnFloor())
		{
			_airJumpCounter = 0;
		} else if (_airJumpCounter == 0)
		{
			_airJumpCounter = 1;	
		}

		if (_airJumpCounter > MaxAirJumps) return;
		if (!Input.IsActionJustPressed("jump")) return;
		var jumpName = "ground_jump";
		if (_airJumpCounter > 0)
		{
			jumpName = "air_jump";
		}
		EmitSignal(nameof(PressedJump), JumpStates[jumpName]);
		_airJumpCounter++;
	}

	public override void _Input(InputEvent @event)
	{
		if (@event.IsAction("movement"))
		{
			//direction.X = Input.GetActionStrength("left") - Input.GetActionStrength("right");
			//direction.Z = Input.GetActionStrength("forward") - Input.GetActionStrength("backward");
		
			var inputDir = Input.GetVector("left","right", "forward", "backward");
			direction.X = inputDir.X;
			direction.Z = inputDir.Y;
		
			if (IsMovement())
			{
				if (Input.IsActionPressed("sprint"))
				{
					EmitSignal(nameof(SetMovementState), MovementStates["sprint"]);
				}
				else
				{
					if (Input.IsActionPressed("walk"))
						EmitSignal(nameof(SetMovementState), MovementStates["walk"]);
					else
						EmitSignal(nameof(SetMovementState), MovementStates["run"]);
				}
			}
			else
			{
				EmitSignal(nameof(SetMovementState), MovementStates["stand"]);
			}
		}

		/*if (@event.IsActionPressed("jump"))
		{
			EmitSignal(nameof(PressedJump), JumpStates["ground_jump"]); 
		}*/
	}
	
	private bool IsMovement()
	{
		return Mathf.Abs(direction.X) > 0 || Mathf.Abs(direction.Z) > 0;
	}
}
